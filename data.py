import api
import sqlite3


class database():
    def __init__(self):
        self.connect()
        self.cursor.execute("""SELECT name FROM sqlite_master WHERE
            type='table' AND name='MIXES'
            """)
        row = self.cursor.fetchone()
        if row is None:
            self.create()

    def connect(self):
        self.conn = sqlite3.connect("mixes.db")
        self.cursor = self.conn.cursor()

    def create(self):
        self.cursor.execute("""CREATE TABLE MIXES
                (name, id, description, playcount, likescount, tag, author)
                """)
        self.conn.commit()

    def update(self):
        mixes = api.api.likedMixes()
        new = []
        for m in mixes:
            new.append((m['name'], m['id'], m['description'], m['plays_count'],
                m['likes_count'], m['tag_list_cache'], m['user']['login']))
        self.cursor.executemany("INSERT INTO MIXES VALUES (?,?,?,?,?,?,?)", new)
        self.conn.commit()

    def likedMixes(self):
        ret = []
        self.cursor.execute("SELECT name FROM MIXES")
        while True:
            row = self.cursor.fetchone()
            if row is None:
                break
            ret.append(row[0])
        return ret

    def idOf(self, name):
        self.cursor.execute("SELECT id FROM MIXES WHERE name='" + name + "'")
        row = self.cursor.fetchone()
        if row is None:
            return -1
        return row[0]

    def infoOf(self, name):
        self.cursor.execute("SELECT * FROM MIXES WHERE name='" + name + "'")
        row = self.cursor.fetchone()
        if row is None:
            return []
        return row

d = database()

import pycurl
import json
import cStringIO
import ConfigParser


class api8tracks():
    def __init__(self):
        self.loadConfig()
        self.auth()
        self.getPlayToken()

    def loadConfig(self):
        Config = ConfigParser.ConfigParser()
        Config.read("p8tracksrc")
        try:
            self.username = Config.get('Account', 'username')
            self.password = Config.get('Account', 'password')
            self.api_key = Config.get('Account', 'api_key')
        except:
            print('Invalid username/password')
            exit(1)

    def auth(self):
        buf = cStringIO.StringIO()
        c = pycurl.Curl()
        c.setopt(c.URL, 'https://8tracks.com/sessions.json')
        c.setopt(c.POSTFIELDS, 'login=' + self.username + '&password='
            + self.password)
        c.setopt(c.WRITEFUNCTION, buf.write)
        c.perform()
        self.user_token = json.loads(buf.getvalue())['user_token']
        buf.close()

    def likedMixes(self):
        buf = cStringIO.StringIO()
        c = pycurl.Curl()
        c.setopt(c.URL, 'https://8tracks.com/mixes.json?view=liked')
        c.setopt(c.HTTPHEADER, ['X-User-Token: ' + str(self.user_token),
            'X-Api-Key: ' + self.api_key])
        c.setopt(c.WRITEFUNCTION, buf.write)
        c.perform()
        self.mixes = json.loads(buf.getvalue())['mixes']
        buf.close()
        return self.mixes

    def getPlayToken(self):
        buf = cStringIO.StringIO()
        c = pycurl.Curl()
        c.setopt(c.URL, 'https://8tracks.com/sets/new.json')
        c.setopt(c.HTTPHEADER, ['X-User-Token: ' + str(self.user_token),
            'X-Api-Key: ' + self.api_key])
        c.setopt(c.WRITEFUNCTION, buf.write)
        c.perform()
        self.play_token = json.loads(buf.getvalue())['play_token']
        buf.close()

    def play(self, mixID):
        buf = cStringIO.StringIO()
        c = pycurl.Curl()
        url = ('https://8tracks.com/sets/' + str(self.play_token) +
                '/play.json?mix_id=' + str(mixID))
        c.setopt(c.URL, url)
        c.setopt(c.HTTPHEADER, ['X-User-Token: ' + str(self.user_token),
            'X-Api-Key: ' + self.api_key])
        c.setopt(c.WRITEFUNCTION, buf.write)
        c.perform()
        resp = json.loads(buf.getvalue())
        buf.close()
        return resp

    def nextTrack(self, mixID):
        buf = cStringIO.StringIO()
        c = pycurl.Curl()
        url = ('https://8tracks.com/sets/' + str(self.play_token) +
                '/next.json?mix_id=' + str(mixID))
        c.setopt(c.URL, url)
        c.setopt(c.HTTPHEADER, ['X-User-Token: ' + str(self.user_token),
            'X-Api-Key: ' + self.api_key])
        c.setopt(c.WRITEFUNCTION, buf.write)
        c.perform()
        resp = json.loads(buf.getvalue())
        buf.close()
        return resp

api = api8tracks()

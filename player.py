import gst
import api
import data


class player():
    def __init__(self):
        self.mixID = 0
        self.player = gst.element_factory_make("playbin", "player")
        self.song = None
        self.state = "Stopped"
        bus = self.player.get_bus()
        bus.enable_sync_message_emission()
        bus.add_signal_watch()
        bus.connect("message", self.on_message)
        #bus.set_sync_handler(on_message)

    def on_message(self, bus, message):
        t = message.type
        if t == gst.MESSAGE_EOS:
            p.nextTrack()
        #return gst.BUS_PASS

    def loadMix(self, name):
        self.stop()
        self.mixID = data.d.idOf(name)
        track = api.api.play(self.mixID)
        self.song = track['set']['track']['name']
        self.player.set_property('uri', track['set']['track']['url'])
        self.play()

    def nextTrack(self):
        self.stop()
        track = api.api.nextTrack(self.mixID)
        self.song = track['set']['track']['name']
        self.player.set_property('uri', track['set']['track']['url'])
        self.play()

    def play(self):
        self.player.set_state(gst.STATE_PLAYING)
        self.state = "Playing"

    def pause(self):
        self.player.set_state(gst.STATE_PAUSED)
        self.state = "Paused"

    def stop(self):
        self.player.set_state(gst.STATE_NULL)
        self.state = "Stopped"

    def toggle(self):
        if self.state == "Paused":
            self.play()
        else:
            self.pause()

p = player()

import curses
import data
import textwrap
import player


class likedMixMenu():
    def __init__(self, h, w):
        self.win = curses.newwin(h - 3, 30, 0, 0)
        self.mixes = data.d.likedMixes()

    def draw(self, pos):
        self.win.erase()
        self.win.border(0)
        self.win.addstr(0, 1, "Liked Mix", curses.A_BOLD)
        for i in range(0, len(self.mixes), 1):
            if i == pos:
                self.win.addstr(2 + i, 1, self.mixes[i], curses.color_pair(1))
            else:
                self.win.addstr(2 + i, 1, self.mixes[i], curses.A_NORMAL)
        self.win.refresh()

    def update(self):
        data.d.update()
        self.mixes = data.d.likedMixes()


class infoMenu():
    def __init__(self, h, w):
        self.w = w
        self.pos = 0
        self.win = curses.newwin(h - 3, w - 30, 0, 30)

    def draw(self, name):
        self.win.erase()
        self.win.border(0)
        self.win.addstr(0, 1, "Info", curses.A_BOLD)
        info = data.d.infoOf(name)
        self.win.addstr(2, 2, "Name:        " + info[0], curses.A_NORMAL)
        self.win.addstr(3, 2, "Author:      " + info[6], curses.A_NORMAL)
        self.win.addstr(5, 2, "Description: ", curses.A_NORMAL)
        description = textwrap.wrap(info[2], self.w - 46)
        self.win.addstr(5, 15, description[0], curses.A_NORMAL)
        i = 1
        for i in range(1, len(description), 1):
            self.win.addstr(5 + i, 15, description[i], curses.A_NORMAL)
        tags = textwrap.wrap(info[5], self.w - 46)
        self.win.addstr(7 + i, 2, "Tags:        " + tags[0], curses.A_NORMAL)
        j = 1
        for j in range(1, len(tags), 1):
            self.win.addstr(7 + i + j, 15, tags[j], curses.A_NORMAL)
        self.win.refresh()


class infoBar():
    def __init__(self, h, w):
        self.win = curses.newwin(3, w, h - 3, 0)

    def draw(self):
        self.win.erase()
        self.win.border(0)
        name = ""
        if player.p.song is not None:
            name = player.p.song
        self.win.addstr(0, 1, player.p.state, curses.A_BOLD)
        self.win.addstr(1, 4, name, curses.A_NORMAL)
        self.win.refresh()


class ncursesUI():
    def __init__(self):
        self.pos = 0
        self.screen = curses.initscr()
        curses.initscr()
        curses.start_color()
        curses.noecho()
        curses.curs_set(0)
        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
        self.screen.keypad(1)
        (h, w) = self.screen.getmaxyx()
        self.leftMenu = likedMixMenu(h, w)
        self.rightMenu = infoMenu(h, w)
        self.bar = infoBar(h, w)

    def draw(self):
        self.leftMenu.draw(self.pos)
        if len(self.leftMenu.mixes) is not 0:
            self.rightMenu.draw(self.leftMenu.mixes[self.pos])
        self.bar.draw()

    def mainLoop(self):
        self.draw()
        ch = self.screen.getch()
        if ch == ord('q'):
            return 0
        if ch == ord('k') and self.pos > 0:
            self.pos -= 1
        elif ch == ord('j') and self.pos < len(self.leftMenu.mixes) - 1:
            self.pos += 1
        elif ch == ord('r'):
            self.leftMenu.update()
        elif ch == ord('o'):
            player.p.loadMix(self.leftMenu.mixes[self.pos])
        elif ch == ord('p'):
            player.p.toggle()
        elif ch == ord('n'):
            player.p.nextTrack()
        return 1
